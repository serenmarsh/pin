let pinNum1 = 0
let pinNum2 = 0

pinNum1 = Number(prompt("Pin setup - Step 1.\nEnter your 4 digit number (1000 to 9999):"))

if (pinNum1 >= 1000 && pinNum1 <= 9999) {
    pinNum2 = Number(prompt("Pin Setup - Step 2.\nPlease re-enter your 4 digit number:"))
    if (pinNum1 === pinNum2){
        console.log("Your 4 digit pin has been set")
    }
    else {
        console.log("Error!\nYour pin numbers do not match. Your pin number was not set.")
    }

} else {
    console.log("This pin is not within the right parameters.")
} 
